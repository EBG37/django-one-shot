from django.urls import path
from todos.views import show_TodoList, detail_Todo, create_todolist
from todos.views import edit_todolist, delete_todolist, create_todoitem
from todos.views import edit_todoitem


urlpatterns = [
    path("items/<int:id>/edit/", edit_todoitem, name="todo_item_update"),
    path("items/create/", create_todoitem, name="todo_item_create"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("<int:id>/edit/", edit_todolist, name="todo_list_update"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/", detail_Todo, name="todo_list_detail"),
    path("", show_TodoList, name="todo_list_list"),
]
