from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def show_TodoList(request):
    todolist = TodoList.objects.all()
    print(todolist)
    context = {
        "key": todolist
    }
    return render(request, "todos/list.html", context)

def detail_Todo(request, id):
    detail = TodoList.objects.get(id=id)
    context = {
        "detail": detail
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
        }
    return render(request, "todos/create.html", context)

def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "post_form": form,
    }
    return render(request, "todos/createitem.html", context)


def edit_todolist(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=post)

        context = {
            "todo_object": post,
            "post_form": form,
        }
        return render(request, "todos/edit.html", context)

def edit_todoitem(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=post)

        context = {
            "todo_object": post,
            "post_form": form,
        }
        return render(request, "todos/edititem.html", context)

def delete_todolist(request, id):
    model = TodoList.objects.get(id=id)
    if request.method == "POST":
        model.delete()
        return redirect("todo_list_list")
    return render(request, "Todos/delete.html")
